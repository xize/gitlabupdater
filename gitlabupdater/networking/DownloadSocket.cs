﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace gitlabupdater.networking
{
    public class DownloadSocket
    {

        private Updater updater;

        public DownloadSocket(Updater updater)
        {
            this.updater = updater;
        }

        public bool FetchDownloadUrls()
        {

            String pattern = "(/"+this.updater.CleanURL + "/uploads/(.*?)/.*?.(zip|checksum|changelog))";

            bool success = false;

            using (WebClient c = new WebClient())
            {

                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

                try
                {
                    String data = c.DownloadString("https://gitlab.com/"+this.updater.LatestURL);
                    MatchCollection col = Regex.Matches(data, pattern, RegexOptions.IgnoreCase);
                    foreach(Match m in col)
                    {
                       

                        if(m.Success)
                        {
                            if(m.Value.EndsWith(".zip"))
                            {
                                this.updater.DownloadString = "https://gitlab.com"+m.Value;
                            } else if(m.Value.EndsWith(".checksum"))
                            {
                                this.updater.DownloadedCheckSum = "https://gitlab.com"+m.Value;
                            } else if(m.Value.EndsWith(".changelog"))
                            {
                                String[] changelog = c.DownloadString("https://gitlab.com"+m.Value).Split('\n');
                                this.updater.Changelog = changelog;
                            }
                        }
                    }
                } catch(Exception e)
                {
                    MessageBox.Show("failed to connect to gitlabs tags page!\n\n"+e.Message, "error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            if(this.updater.DownloadString.Length > 0)
            {
                success = true;
            }

            return success;
        }

    }
}
