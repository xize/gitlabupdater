﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace gitlabupdater.networking
{
    public class FindReleaseSocket
    {

        private Updater updater;

        public FindReleaseSocket(Updater updater)
        {
            this.updater = updater;
        }

        public bool SearchForRelease()
        {

            String pattern = this.updater.GetFullRepository()+"/-/tags/v";

            String newversion = this.updater.GetCurrentVersion();

            while (true)
            {
                String oldver = newversion;

                using (WebClient client = new WebClient())
                {
                    ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

                    try
                    {
                        newversion = this.updater.GetOctalCalculator().IncrementVersion(newversion);
                        client.DownloadString(pattern+newversion);
                    }
                    catch (WebException e)
                    {
                        if (((HttpWebResponse)e.Response).StatusCode == HttpStatusCode.NotFound)
                        {
                            newversion = oldver;
                            break;
                        }
                    }
                }
            }

            if(newversion != this.updater.GetCurrentVersion())
            {
                this.updater.LatestURL = this.updater.CleanURL+"/-/tags/v" + newversion;
                this.updater.NewVersion = newversion;
                return true;
            }

            return false;
        }

    }
}
