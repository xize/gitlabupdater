# A library to make updating programs easier from gitlab.

#### how to use?
Since gitlab does not support the /release/latest tag, it is alot harder to fetch the correct page and download the latest version of your program.

usage is very simple:

```
        [STAThread]
        static void Main(String[] args)
        {

            Updater update = new Updater("xize/borderlands2-trainer", "1.9.0.4", "1920EED5A821897E844454AA853F2CB706318EABB7EDBFC11352B5CA59369EFF"); //current hash

            Console.WriteLine("searching for updates...");
            if(update.Verify())
            {
                Console.WriteLine("update found last version: "+update.LatestVersion+"!");
                Console.WriteLine("downloading file...");
                update.Download();
            } else
            {
                Console.WriteLine("failed to find updates!");
            }

            Console.ReadKey();
        }
```

This will download the first zip it found and searches also in the same repo for a file called ```<any file.checksum>```,
this checksum needs to be newer than the checksum inside the program which then gets compared to the checksum inside the program.

if there is no checksum found it will not check against a checksum but warns the user.